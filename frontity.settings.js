const settings = {
  name: "forgotten-developer",
  state: {
    frontity: {
      url: "https://giodc.com",
      title: "decode",
      description: "A retired developer back in action",
    },
    sitemap: {
      origin: "https://api.divaksh.com/post-sitemap.xml"
    }
  },
  packages: [
    {
      name: "forgotten-developer",
      state: {
        theme: {
          menu: [
            ["Home", "/"],
            ["Woo", "/c/woocommerce/"],
            ["WP", "/c/wordpress/"],
            ["jQ", "/http/"],
            ["?", "/java/"],
          ],
          featured: {
            showOnList: true,
            showOnPost: true,
          },
        },
      },
    },
    {
      name: "@frontity/wp-source",
      state: {
        source: {
          api: "https://visualounge.com/wp-json",
        },
      },
    },
    {
      name: "@frontity/google-analytics",
      state: {
          googleAnalytics: {
              trackingId: ''
          },
      },
  },
    "@frontity/tiny-router",
    "@frontity/html2react",
    "@frontity/head-tags",
    "@frontity/yoast",
    "@frontity/wp-comments",
  ],
};

export default settings;
